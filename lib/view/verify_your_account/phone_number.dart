import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class VerifyYourAccount extends StatefulWidget {
  const VerifyYourAccount({super.key});

  @override
  State<VerifyYourAccount> createState() => _VerifyYourAccountState();
}

class _VerifyYourAccountState extends State<VerifyYourAccount> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0,
        automaticallyImplyLeading: false,
        title: Text(
          "Enter your phone number",
          style: GoogleFonts.poppins(
              color: Colors.white,
              fontWeight: FontWeight.w200,
              fontSize: 18,
              letterSpacing: -.5,
              wordSpacing: .1),
        ),
        centerTitle: true,
        actions: [
          PopupMenuButton(
            constraints: const BoxConstraints(maxHeight: 100),
            iconColor: Colors.white70,
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: Text(
                    "Link as companion device",
                    style: GoogleFonts.poppins(),
                  ),
                ),
                PopupMenuItem(
                  padding: const EdgeInsets.only(bottom: 10, left: 12),
                  child: Text(
                    "Help",
                    style: GoogleFonts.poppins(),
                  ),
                )
              ];
            },
          )
        ],
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(
                top: 10,
              ),
              child: RichText(
                text: TextSpan(
                  style: GoogleFonts.poppins(
                      fontSize: 11, fontWeight: FontWeight.w200),
                  children: [
                    const TextSpan(text: "WhatsApp will need to verify account."),
                    TextSpan(
                      text: "  What's my number?",
                      style: GoogleFonts.poppins(color: Colors.blue.shade600),
                    )
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 165, top: 20),
              child: MaterialButton(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                //hoverColor: Colors.transparent,
                onPressed: () {},
                child: Center(
                  child: Row(
                    children: [
                      Text(
                        "India",
                        style: GoogleFonts.poppins(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.w200),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 100),
                        child: const Icon(
                          Icons.arrow_drop_down,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 90, right: 90),
              child: TextField(
                textAlign: TextAlign.center,
                cursorColor: Colors.teal,
                decoration: InputDecoration(
                    hoverColor: Colors.greenAccent,
                    focusColor: Colors.greenAccent,
                    focusedBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.teal, width: 2)),
                    enabledBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.teal, width: 2)),
                    border: const UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.teal, width: 2)),
                    hintText: "phone number",
                    hintStyle: GoogleFonts.poppins(
                        color: Colors.white,
                        fontWeight: FontWeight.w100,
                        letterSpacing: -.2)),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(top: 10),
              child: Text(
                "International carrier charges may apply.",
                style: GoogleFonts.poppins(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.w100,
                  letterSpacing: -.1
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
