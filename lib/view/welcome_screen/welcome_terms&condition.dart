import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../verify_your_account/phone_number.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({super.key});

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  // final Uri _privacypolicy =
  //     Uri.parse("https://www.whatsapp.com/legal/privacy-policy");
  // final Uri _tos = Uri.parse("https://www.whatsapp.com/legal/terms-of-service");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(
            height: 145,
          ),
          Container(
            width: 300,
            height: 300,
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage("assets/icons/whatsapp_background.jpg"),
              ),
            ),
          ),
          const SizedBox(
            height: 40,
          ),
          Text(
            "Welcome to WhatsApp",
            style: GoogleFonts.poppins(fontSize: 25, color: Colors.white),
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            "Read our Privacy Policy. Tap \"Agree and continue\" to accept the",
            style: GoogleFonts.poppins(
                fontSize: 12, color: Colors.white, fontWeight: FontWeight.w200),
          ),
          Text(
            "Terms of Service",
            style: GoogleFonts.poppins(
                fontSize: 12, color: Colors.white, fontWeight: FontWeight.w200),
          ),
          Container(
            padding: const EdgeInsets.only(left: 130, right: 130, top: 20),
            child: MaterialButton(
              height: 40,
              shape: const StadiumBorder(),
              color: Colors.white10,
              onPressed: () {},
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  const Icon(
                    Icons.language,
                    color: Colors.white,
                  ),
                  Text(
                    "English",
                    style: GoogleFonts.poppins(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.w200),
                  ),
                  const Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 230),
            child: MaterialButton(
              minWidth: 390,
              height: 50,
              shape: const StadiumBorder(),
              color: Colors.tealAccent,
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const VerifyYourAccount()));
              },
              child: Text(
                "Agree and Continue",
                style: GoogleFonts.poppins(fontWeight: FontWeight.w300),
              ),
            ),
          )
        ],
      ),
    );
  }
}
