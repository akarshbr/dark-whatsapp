import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../welcome_screen/welcome_terms&condition.dart';

class Splash extends StatefulWidget {
  const Splash({super.key});

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    Timer(const Duration(seconds: 3), () {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => const WelcomeScreen()));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        padding: const EdgeInsets.only(top: 400),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "assets/icons/whatsapp_icon.png",
              height: 80,
              width: 80,
            ),
            Container(
              padding: const EdgeInsets.only(top: 360),
              child: Text(
                "from",
                style: GoogleFonts.poppins(color: Colors.white54, fontSize: 15, letterSpacing: 1),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/icons/meta_icon.png",
                  height: 25,
                  width: 25,
                ),
                Text(
                  " Meta",
                  style: GoogleFonts.poppins(color: Colors.white, fontSize: 25),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
